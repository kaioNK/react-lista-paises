import React, {Component} from 'react'
import { Container, Row, Col, Navbar, NavbarBrand, InputGroup, InputGroupAddon, InputGroupText, Input, Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle } from 'reactstrap';
import axios from 'axios'

const URL = "https://restcountries.eu/rest/v2/all"

class Main extends Component{
  state = {
    data: []
  }

  componentDidMount(){
    axios.get(URL).then(res => {
      const data = res.data 
      this.setState({data})
    })
  }

  teste = (cols) => {
    console.log("Length: ", this.state.data)
    let x = Math.ceil(this.state.data.length / cols)
    console.log("Rows: ", x)
  }

  render(){
    const {data} = this.state
    this.teste(4)
    return(
      <div>
         <Navbar className='bg-dark'>
          <NavbarBrand><h3>Where in the world</h3></NavbarBrand>
        </Navbar>
        <Container>
          <Row>
            <Col xs="8">
              <InputGroup>
                <Input placeholder="Search for a country..." />
              </InputGroup>
            </Col>
          </Row>
          {data.map(item => {
            return(
              <Row>
                <Col key={item.id}>
                  <Card>
                    <CardImg top width="100%" src={item.flag} alt="Card image cap" />
                    <CardBody>
                    <CardTitle>{item.name}</CardTitle>
                    <CardText>Population: {item.population}</CardText>
                    <CardText>Region: {item.region}</CardText>
                    <CardText>Capital: {item.capital}</CardText>
                    </CardBody>
                  </Card>
                </Col> 
                
              </Row>
          )
        })} 
      </Container>
      </div>
      
    )
  }
}

export default Main